from django import forms
from .models import Testimoni

class TestForm(forms.ModelForm):
    message = forms.CharField(max_length=300, widget=forms.Textarea(
        attrs={
            'rows': 5,
            'cols': 100,
            'placeholder': 'Type in your message...',
            'id': 'text',
            'style':'overflow: hidden; word-wrap: break-word; resize: none; height: 160px;',
        }
    ))

    class Meta:
        model = Testimoni
        fields = ('message',)
