from django.db import models

class Testimoni(models.Model):
    message =  models.CharField(max_length=300)
    name = models.CharField(max_length=300)
    time = models.DateTimeField(auto_now_add=True)
# Create your models here.
