from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Testimoni
from .forms import TestForm
from .views import aboutus
from django.contrib.auth.models import User
from importlib import import_module
from TP1 import settings
import unittest
# Create your tests here.

class Ab5UnitTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()

    def test_ab5_url_is_exist(self):
        response = Client().get('/aboutus/')
        self.assertEqual(response.status_code, 200)

    def test_model_check_items_in_model(self):
        Testimoni.objects.create(message='test',)
        counting_all_available_todo = Testimoni.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_page_using_daftardonatur_func(self):
        found = resolve('/aboutus/')
        self.assertEqual(found.func, aboutus)

    def test_no_login_no_form(self):
        response = Client().get('/aboutus/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('<button class="btn btn-submit" type="submit" id="button"> Submit </button>', html_response)

#    def test_if_login_form_yes(self):
#        self.user = User.objects.create(username='testuser', password='12345')
#        self.user.set_password('hello')
#        self.user.save()
#        self.client.login(username='testuser', password='hello')
#        s = self.client.session
#        s['email'] = "adadeh@gmail.com"        Pengimplementasian test telah menggunakan session, tetapi terdapat keyerror saat di views...
#        s['name'] = "hehehe"
#        s.save()
#        request.session['name'] = 'name';
#        response = Client().get('/aboutus/')
#        html_response = response.content.decode('utf8')
#        self.assertIn('<button class="btn btn-submit" type="submit" id="button"> Submit </button>', html_response)

#    def test_post_success(self):
#        self.user = User.objects.create(username='coba', password='12345')
#        self.user.set_password('hello')
#        self.user.save()
#        self.client.login(username='coba', password='hello')
#        s = self.client.session
#        s['email'] = "adadeh@gmail.com"
#        s['name'] = "hehehe"
#        s.save()
#        request.session['name'] = 'name';
#        response_post = Client().post('/aboutus/', {'message': 'test',})
#        counting_all_available_todo = Testimoni.objects.all().count()
#        self.assertEqual(counting_all_available_todo, 1)
