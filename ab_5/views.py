from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse

from .forms import TestForm
from .models import Testimoni


# Create your views here.
def aboutus(request):
    if request.method == 'POST':
        if 'name' not in request.session:
            HttpResponseRedirect(reverse('ab_4:daftardonatur'))
        form = TestForm(request.POST)
        if form.is_valid():
            message = request.POST['message']
            obj = Testimoni.objects.create(message=message, name=request.session['name'])
            time = obj.time.strftime("%b. %d, %Y, %I:%M %p")
            return JsonResponse({'message': message, 'name': request.session['name'], 'time': time})
    else:
        if 'name' not in request.session:
            messages = Testimoni.objects.all()
            return render(request, 'ab_5/aboutus.html', {'messages': messages})
        else:
            form = TestForm()
            messages = Testimoni.objects.all()
            return render(request, 'ab_5/aboutus.html', {'form': form, 'messages': messages})
