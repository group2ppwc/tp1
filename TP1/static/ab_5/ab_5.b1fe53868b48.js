
$('form').on('submit', function (e) {
    e.preventDefault();
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/aboutus/",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: $("form").serialize(),
        dataType: "json",
        success: function(result) {
            console.log(result.name);
            var message = '<h4>' + result.name + '</h4>posted on ' + result.time + '<h5>' + result.message + '</h5>';
            $(".input").append(message);
        }
    });
});
