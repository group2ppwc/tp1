var listDonation = function(email) {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: "POST",
            url: "/profile",
            headers:{
                "X-CSRFToken": csrftoken
            },
            data: {email: email },
            success: function(result) {
                var html = "<h4 class=\"modal-title\">" + "Jumlah Donasi: " + result.total +"</h4>";
                $('.modal-title').replaceWith(html);
                var list = result.list;
                var start = $('.start');
                start.empty();
                for (var x = 0; x < list.length; x++) {
                    start.append("<div class=\"p-2\">" + list[x].judul + ": Rp" + list[x].donasi + "</div>")
                }
            },
            error: function (error) {
                alert("Something error, please report")
            }
        })
    };