"""TP1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from ab_1.views import homepage
from Profile.views import profile
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', homepage, name='index'),
    path('program/', include('ab_3.urls')),
    path('homepage/', include('ab_1.urls')),
    path('daftardonatur/', include('ab_4.urls')),
    path('donasikan/', include('ab_2.urls')),
    path('aboutus/', include('ab_5.urls')),
    path('auth/', include('social_django.urls', namespace='social')),  # <- Here
    path('profile', profile, name='profile')
]
