from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import daftardonatur
from .models import Profile
from .forms import HomeForm

# Create your tests here.
class Ab4UnitTest(TestCase):
    def test_ab4_url_is_exist(self):
        response = Client().get('/daftardonatur/')
        self.assertEqual(response.status_code, 200)

    def test_model_check_items_in_model(self):
        Profile.objects.create(first_name='test', last_name='test', birthDay='2018-01-01', email='coba@hai.com', password='cobaaja')
        counting_all_available_todo = Profile.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_model_check_items_in_model_double_email(self):
        test = 'anonymous'
        coba = '2018-01-01'
        email = 'coba@hai.com'
        Client().post('/daftardonatur/', {'first_name': test, 'last_name': test, 'birthDay': coba, 'email': email, 'password': test})
        counting_all_available_todo = Profile.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)
        Client().post('/daftardonatur/', {'first_name': test, 'last_name': test, 'birthDay': coba, 'email': email, 'password': test})
        counting_all_available_todo = Profile.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_post_success(self):
        test = 'anonymous'
        coba = '2018-01-01'
        email = 'coba@hai.com'
        response_post = Client().post('/daftardonatur/', {'first_name': test, 'last_name': test, 'birthDay': coba, 'email': email, 'password': test})
        self.assertEqual(response_post.status_code, 302)

#    def test_post_email_has_been_taken(self):
#        text = 'E-mail has already been taken'
#        test = 'anonymous'
#        coba = '2018-01-01'
#        email = 'coba@hai.com'
#        response_post = Client().post('/daftardonatur/', {'first_name': test, 'last_name': test, 'birthDay': coba, 'email': email, 'password': test})
#        response_post = Client().post('/daftardonatur/', {'first_name': test, 'last_name': test, 'birthDay': coba, 'email': email, 'password': test})
#        html_response = response_post.content.decode('utf8')
#        self.assertIn(text, html_response)

    def test_page_using_daftardonatur_func(self):
        found = resolve('/daftardonatur/')
        self.assertEqual(found.func, daftardonatur)
