from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import HomeForm
from .models import Profile

# Create your views here.
def daftardonatur(request):
    if request.method == 'POST':
        form = HomeForm(request.POST)
        if form.is_valid():
            first_name = request.POST['first_name']
            last_name = request.POST['last_name']
            birthDay = request.POST['birthDay']
            email = request.POST['email']
            password = request.POST['password']
            existing_user = Profile.objects.filter(email=form.cleaned_data.get('email')).first()
            if existing_user:
                messages.info(request, 'E-mail has already been taken')
                return HttpResponseRedirect(reverse('ab_4:daftardonatur'))
            else:
                Profile.objects.create(first_name=first_name, last_name=last_name, birthDay=birthDay, email=email, password=password)
                return HttpResponseRedirect(reverse('ab_1:homepage'))
    else:
        form = HomeForm()
        return render(request, 'ab_4/daftardonatur.html', {'form': form})
