from django.urls import path
from . import views

app_name = "ab_4"

urlpatterns = [
    path('', views.daftardonatur, name="daftardonatur"),
]
