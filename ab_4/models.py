from django.db import models

class Profile(models.Model):
    first_name = models.CharField(max_length=300)
    last_name = models.CharField(max_length=300)
    birthDay = models.DateField()
    email = models.EmailField()
    password = models.CharField(max_length=300)
