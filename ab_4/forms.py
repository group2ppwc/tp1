from django import forms
from .models import Profile

class HomeForm(forms.ModelForm):
    first_name = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'First Name',
        }
    ))
    last_name = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Last Name',
        }
    ))
    birthDay = forms.DateField(widget=forms.DateInput(
        attrs={
            'type': 'text',
            'onfocus': '(this.type="date")',
            'onblur': '(this.type="text")',
            'class': 'form-control',
            'placeholder': 'Birthday',
        }
    ), help_text='Please enter your Birthday')
    email = forms.EmailField(widget=forms.EmailInput(
        attrs={
            'type': 'email',
            'class': 'form-control',
            'placeholder': 'E-mail',
        }
    ))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'type': 'password',
            'class': 'form-control',
            'placeholder': 'Password',
        }
    ))

    class Meta:
        model = Profile
        fields = ('first_name', 'last_name', 'birthDay', 'email', 'password',)
