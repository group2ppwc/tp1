from django.shortcuts import render
from django.http import JsonResponse
from ab_2.models import ProgramData
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required
def profile(request):
    email = request.session['email']
    nama = request.session['name']
    if request.method == "POST":
        emailPost = request.POST['email']
        listProgram = ProgramData.objects.all().filter(email=emailPost)
        total = 0
        list = []
        for data in listProgram:
            total += data.donation
            newdict = {"judul": data.program.judul, "donasi": data.donation}
            list.append(newdict)
        return JsonResponse({'nama': nama, 'list': list, 'total': total})
    return render(request, 'Profile/Profile.html', {'name': nama, 'email': email})