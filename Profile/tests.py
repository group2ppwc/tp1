from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.test import TestCase
from importlib import import_module
from django.conf import settings
# Create your tests here.


class SessionTestCase(TestCase):
    def setUp(self):
        # http://code.djangoproject.com/ticket/10899
        settings.SESSION_ENGINE = 'django.contrib.sessions.backends.file'
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key


class ProfileSessionTest(SessionTestCase):
    def test_profile_redirect_not_logged_in(self):
        response = self.client.get('/profile')
        self.assertEqual(response.status_code, 302)

    def test_profle_logged_in(self):
        self.user = User.objects.create(username='testuser', password='12345')
        self.user.set_password('hello')
        self.user.save()
        self.client.login(username='testuser', password='hello')
        s = self.client.session
        s['email'] = 'test@test.com'
        s['name'] = 'lala'
        s.save()
        response = self.client.get('/profile')
        self.assertEqual(response.status_code, 200)