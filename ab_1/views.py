from django.shortcuts import render
from ab_3.models import Program
from .models import News
from django.contrib.auth import logout as auth_logout
from django.http import HttpResponseRedirect
# Create your views here.
def homepage(request):
    response = {}
    response['news'] = News.objects.all()
    response['donationPrograms'] = Program.objects.all()
    if request.user.is_authenticated:
        if 'username' not in request.session:
            request.session['username'] = request.user.username
        if 'email' not in request.session:
            request.session['email'] = request.user.email
        if 'name' not in request.session:
            request.session['name'] = request.user.first_name + " " + request.user.last_name
            print(request.session['name'])
    return render(request, 'ab_1/homepage.html', response)

def logout(request):
    auth_logout(request)
    request.session.flush()
    return HttpResponseRedirect('/')