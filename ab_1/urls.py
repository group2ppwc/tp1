from django.urls import path
from .views import *

app_name = "ab_1"

urlpatterns = [
    path('', homepage, name="homepage"),
    path('logout', logout, name="logout"),
]