from django.test import TestCase, Client
from .models import News
from ab_3.models import Program
from django.urls import resolve
from django.contrib.auth.models import User
from .views import *
# Create your tests here.
class ab1UnitTest(TestCase):
    def test_ab1_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_ab1_has_news_models(self):
        News.objects.create(judul="test", snippet="abscdlre", gambar="https://badmintonindonesia.org/upload/news/boxa73e29e1-57e9-4435-af50-feaa8d4cfbf1.jpg", readmore="https://badmintonindonesia.org/app/information/newsDetail.aspx?/7481")
        count_all_stats = News.objects.all().count()
        self.assertEqual(count_all_stats, 1)
    def test_ab1_using_homepage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)
    def test_ab1_show_news(self):
        News.objects.create(judul="test", snippet="abscdlre", gambar="https://badmintonindonesia.org/upload/news/boxa73e29e1-57e9-4435-af50-feaa8d4cfbf1.jpg", readmore="https://badmintonindonesia.org/app/information/newsDetail.aspx?/7481")
        response = Client().get('/')
        news_content = "ApaKabar"
        html_response = response.content.decode('utf8')
        self.assertIn(news_content,html_response)
    def test_ab1_show_donation_program(self):
        news_judul = "salkfnnka"
        news_isi = "define"
        news_gambar = "b.jpg"
        Program.objects.create(judul=news_judul, targetDonasi=124999, donasiAwal=0, isi=news_isi, gambar=news_gambar)
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(news_judul ,html_response)
        self.assertIn(news_isi, html_response)
        self.assertIn(news_gambar, html_response)

    ''' Unit Test for TP2'''
    def test_login_ab1_and_has_session(self):
        c = Client()
        c.force_login(User.objects.get_or_create(username='testuser')[0])
        home = c.get('/')
        self.assertTrue(home.context['user'].is_authenticated)
        session = dict(c.session)
        self.assertTrue("username" in session)
        self.assertTrue("email" in session)
        self.assertTrue("name" in session)
        response = c.get('/')
        self.assertIn("Log Out", response.content.decode('utf8'))
    
    def test_logout(self):
        c = Client()
        c.force_login(User.objects.get_or_create(username='testuser')[0])
        c.logout()
        home = c.get('/')
        self.assertFalse(home.context['user'].is_authenticated)
        session = dict(c.session)
        self.assertFalse("username" in session)
        self.assertFalse("email" in session)
        self.assertFalse("name" in session)
        response = c.get('/')
        self.assertIn("Daftar Donatur", response.content.decode('utf8'))



        
