# Generated by Django 2.1.1 on 2018-10-16 07:18

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('judul', models.CharField(max_length=100)),
                ('snippet', models.CharField(max_length=500)),
                ('gambar', models.CharField(max_length=200)),
            ],
        ),
    ]
