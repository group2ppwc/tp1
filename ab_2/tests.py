from importlib import import_module

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import resolve
from ab_3.models import Program
from ab_4.models import Profile
from .views import form
from .models import ProgramData
from django.conf import settings

# Create your tests here.


class Ab2UnitTest(TestCase):
    def test_model_can_make_donation(self):
        program = Program.objects.create(judul='test', targetDonasi=2000, donasiAwal=0, isi="testetstetstet", gambar='a.jpg')
        ProgramData.objects.create(program=program,
                                   name='jack',
                                   email='jack@jack.com',
                                   donation=1000)
        count = ProgramData.objects.all().count()
        self.assertEqual(count, 1)

    def test_ab2_using_form_function(self):
        found = resolve('/donasikan/test')
        self.assertEqual(found.func, form)


class SessionTestCase(TestCase):
    def setUp(self):
        # http://code.djangoproject.com/ticket/10899
        settings.SESSION_ENGINE = 'django.contrib.sessions.backends.file'
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key


class Ab2SessionTest(SessionTestCase):

    def test_donation_form_redirect_not_logged_in(self):
        Program.objects.create(judul='test', targetDonasi=2000, donasiAwal=0, isi="testetstetstet", gambar='a.jpg')
        response = self.client.get('/donasikan/test')
        self.assertEqual(response.status_code, 302)

    def test_donation_form_logged_in(self):
        self.user = User.objects.create(username='testuser', password='12345')
        self.user.set_password('hello')
        self.user.save()
        self.client.login(username='testuser', password='hello')
        s = self.client.session
        s['email'] = "adadeh@gmail.com"
        s['name'] = "hehehe"
        s.save()
        Program.objects.create(judul='test', targetDonasi=2000, donasiAwal=0, isi="testetstetstet", gambar='a.jpg')
        response = self.client.get('/donasikan/test')
        self.assertEqual(response.status_code, 200)

    def test_post_success(self):
        s = self.client.session
        s['email'] = "adadeh@gmail.com"
        s['name'] = "hehehe"
        s.save()
        tanggal = '2018-01-01'
        Profile.objects.create(first_name=s['name'], last_name=s['name'], birthDay=tanggal, email=s['email'], password=s['name'])
        Program.objects.create(judul='test', targetDonasi=2000, donasiAwal=0, isi="testetstetstet", gambar='a.jpg')
        response = self.client.post('/donasikan/test', {'name': s['name'], 'email': s['email'], 'donation': 123})
        self.assertEqual(response.status_code, 302)

    def test_anonymous(self):
        self.user = User.objects.create(username='testuser', password='12345')
        self.user.set_password('hello')
        self.user.save()
        self.client.login(username='testuser', password='hello')
        s = self.client.session
        s['email'] = "adadeh@gmail.com"
        s['name'] = "hehehe"
        s.save()
        tanggal = '2018-01-01'
        Profile.objects.create(first_name=s['name'], last_name=s['name'], birthDay=tanggal, email=s['email'], password=s['name'])
        Program.objects.create(judul='test', targetDonasi=2000, donasiAwal=0, isi="testetstetstet", gambar='a.jpg')
        self.client.post('/donasikan/test', {'name': s['name'], 'email': s['email'], 'donation': 123, 'jangan_tampilkan_nama_saya' : True})
        listDonatur = ProgramData.objects.all()
        self.assertEqual(listDonatur.count(), 1)
        donatur = listDonatur[0]
        self.assertEqual(donatur.name, 'anonim')

    def test_ab2_url_is_exist(self):
        self.user = User.objects.create(username='testuser', password='12345')
        self.user.set_password('hello')
        self.user.save()
        self.client.login(username='testuser', password='hello')
        s = self.client.session
        s['email'] = "adadeh@gmail.com"
        s['name'] = "hehehe"
        s.save()
        program = Program.objects.create(judul='test', targetDonasi=2000, donasiAwal=0, isi="testetstetstet", gambar='a.jpg')
        ProgramData.objects.create(program=program,
                                   name='jack',
                                   email='jack@jack.com',
                                   donation=1000)
        response = self.client.get('/donasikan/test')
        self.assertEqual(response.status_code, 200)