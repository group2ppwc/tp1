from django.urls import path
from .views import form

app_name = "ab_2"

urlpatterns = [
    path('<str:judul>', form)
]
