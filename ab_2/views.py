from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import DonasiForm
from .models import ProgramData
from ab_3.models import Program
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.contrib.auth.decorators import login_required

response = {}


@login_required
def form(request, judul):
    program = get_object_or_404(Program, pk=judul)
    DonasiForm.program = program.judul
    if request.method == 'POST':
        form = DonasiForm(request.POST)
        if form.is_valid():
            if request.POST.get('jangan_tampilkan_nama_saya', False):
                response['name'] = 'anonim'
            else:
                response['name'] = request.session['name']
            response['email'] = request.session['email']
            response['donation'] = form.cleaned_data['donation']
            ProgramData.objects.create(program=program, name=response['name'],
                                       email=response['email'],
                                       donation=response['donation'])
            program.donasiAwal = program.donasiAwal + int(response['donation'])
            program.save()
            return HttpResponseRedirect(reverse('ab_1:homepage'))
    response['form'] = DonasiForm
    response['judul'] = program.judul
    response['nama'] = request.session['name']
    response['email'] = request.session['email']
    return render(request, 'ab_2/donasikan.html', response)


