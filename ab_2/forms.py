from django import forms
from .models import ProgramData
from django.core.validators import MinValueValidator, MaxValueValidator


class DonasiForm(forms.ModelForm):
    donation = forms.IntegerField(required=True,
                                  widget=forms.NumberInput(
                                      attrs={'class': 'form-control',
                                             'type':'number',
                                             'placeholder': 'Donation Amount',}
                                      ),
                                  validators=[MinValueValidator(1), MaxValueValidator(1000000000000)])

    jangan_tampilkan_nama_saya = forms.BooleanField(required=False)
        
        
    class Meta:
        model = ProgramData
        fields = ('donation',)
