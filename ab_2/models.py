from django.db import models
from ab_3.models import Program


class ProgramData(models.Model):
    program = models.ForeignKey(Program, on_delete=models.CASCADE)
    name = models.CharField(max_length=300)
    email = models.EmailField()
    donation = models.IntegerField()