from django.shortcuts import render
from .models import Program
from django.shortcuts import get_object_or_404
from ab_2.models import ProgramData
# Create your views here.


def ProgramView(request, judul):
    program = get_object_or_404(Program, pk=judul)
    listDonatur = ProgramData.objects.all().filter(program=program)
    percent = program.donasiAwal/program.targetDonasi*100
    sisa = 100 - percent
    return render(request, 'ab_3/program.html', {'judul': program.judul, 'target': program.targetDonasi,
                                                 'sekarang':program.donasiAwal, 'isi':program.isi,
                                                 'foto': program.gambar,
                                                 'percent': "{0:.2f}".format(percent),
                                                 'sisa': "{0:.2f}".format(sisa),
                                                 'listDonatur': listDonatur, 'donatur': listDonatur.count()})
