from django.db import models

# Create your models here.

class Program(models.Model):
    judul = models.CharField(max_length=100, primary_key=True)
    targetDonasi = models.DecimalField(max_digits=20, decimal_places=0, default=0)
    donasiAwal = models.DecimalField(max_digits=20, decimal_places=0, default=0)
    isi = models.TextField(default='halo')
    gambar = models.CharField(max_length=200, default='http://cdn2.tstatic.net/tribunnews/foto/bank/images/gempa-palu_20181005_153426.jpg')


