from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import ProgramView
from .models import Program

# Create your tests here.


class Ab3UnitTest(TestCase):
    def test_model_can_create_new_program(self):
        Program.objects.create(judul='test', targetDonasi=2000, donasiAwal=0, isi="testetstetstet", gambar='a.jpg')
        count = Program.objects.all().count()
        self.assertEqual(count, 1)

    def test_ab3_url_is_exist(self):
        Program.objects.create(judul='test', targetDonasi=2000, donasiAwal=0, isi="testetstetstet", gambar='a.jpg')
        response = Client().get('/program/test')
        self.assertEqual(response.status_code, 200)

    def test_ab3_using_ProgramView_func(self):
        found = resolve('/program/test')
        self.assertEqual(found.func, ProgramView)

    def test_ab3_render_result(self):
        judul = 'aab'
        targetDonasi='2000'
        donasiAwal='0'
        isi = "testetstetstet"
        Program.objects.create(judul='aab', targetDonasi=2000, donasiAwal=0, isi="testetstetstet", gambar='a.jpg')
        response = Client().get('/program/aab')
        html_response = response.content.decode('utf8')
        self.assertIn(judul, html_response)
        self.assertIn(targetDonasi, html_response)
        self.assertIn(donasiAwal, html_response)
        self.assertIn(isi, html_response)

    def test_ab3_using_template(self):
        Program.objects.create(judul='aab', targetDonasi=2000, donasiAwal=0, isi="testetstetstet", gambar='a.jpg')
        response = Client().get('/program/aab')
        self.assertTemplateUsed(response, 'ab_3/program.html')