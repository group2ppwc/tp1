from django.urls import path
from .views import ProgramView

app_name = "ab_3"

urlpatterns = [
    path('<str:judul>', ProgramView, name='program')
]
