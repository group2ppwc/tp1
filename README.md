# Tugas 1 PPW

## Status
![coverage report](https://gitlab.com/group2ppwc/tp1/badges/master/coverage.svg)
![pipeline](https://gitlab.com/group2ppwc/tp1/badges/master/build.svg)

## Anggota Kelompok
- Akhmad Ramadhan Yamin (1706043834)
- Edrick Lainardi (1706040113)
- Fakhira Devina (1706979221)
- Michael Wiryadinata Halim (1706039944)

## Link Heroku
http://ayobantu.herokuapp.com/
